module.exports = async(app, options = {}, { getPages }) => {
    return {
        context: {
            pages: (await getPages(options))
                .filter(p => {
                    return !!p.data.article
                })
                .sort((a, b) => {
                    if (
                        a.article &&
                        a.article.created &&
                        b.article &&
                        b.article.created
                    ) {
                        return a.article.created._d.getTime() >
                            b.article.created._d.getTime() ?
                            -1 :
                            1
                    } else {
                        return true
                    }
                })
        }
    }
}