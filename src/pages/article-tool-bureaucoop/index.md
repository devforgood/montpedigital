Nous présentons un outil pour trouver un lieu de travail lorsque nous sommes des travailleurs du numérique. Les personnes qui vivent dans une ville et travaillent à la maison peuvent offrir un bureau et de la nourriture à d’autres travailleurs.

L'objectif de cette plate-forme est d'éviter de payer 300 EUR pour un espace de coworking tel que Regus, tout en rendant le travail quotidien à la maison plus amusant.

![](https://i.imgur.com/VNC7bBS.png)

Nous commençons tout juste à développer cet outil et nous avons besoin de contributeurs pour se concrétiser. Si vous êtes intéressé, vous savez. Nous sommes ici!

#

**Repository**

https://framagit.org/javimosch/bureaucoop

**Staging version**

https://edge.bureaucoop.misitioba.com

**Stable version**

https://bureaucoop.misitioba.com
