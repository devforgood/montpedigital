module.exports = async app => {
    return {
        layout: 'blog',
        head_title: 'Exploring Montpesites, a decentralized self-hosted tool',
        title: `Exploring Montpesites, a decentralized self-hosted static generation tool for developers`,
        metaKeywords: `self-hosted decentralized framasoft logiciel libre`,
        target: '/exploring-montpesites-decentralized-self-hosted-ssg-tool-for-devs',
        format: 'md',
        article: {
            author: '@javimosch',
            created: app.helpers.moment('20-09-2019 10:20', 'DD-MM-YYYY HH:mm')
        }
    }
}